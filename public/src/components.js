/**
 * @file App build all components.
 */
import { define } from '../web_modules/hybrids.js';
import components from './components/index.mjs';
import cvData from '../cv_data.mjs';

Object.entries(components(cvData)).forEach(([tagName, element]) => {
  define(tagName, element);
});
