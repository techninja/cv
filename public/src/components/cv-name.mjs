/**
 * @file CV Name component.
 */
import { html } from '../../web_modules/hybrids.js';

export default ({ names }) => ({
  render: () => html`
    <style>
      figure {
        text-align: center;
        font-size: 4.5em;
        margin: 0;
        color: var(--text-color) !important;
      }
      h1 {
        font-family: 'Oswald', sans-serif;
        font-size: 1em;
        font-weight: 100;
        text-transform: uppercase;
        margin: 0;
        margin-bottom: -0.2em;
        white-space: nowrap;
      }
      h1 em {
        font-style: normal;
        font-weight: 900;
      }
      aside {
        font-family: 'Inter';
        font-size: 0.48em;
      }

      @media only screen and (max-width: 700px) {
        figure {
          margin-right: 1em;
          margin: 0;
          width: 100%;
          font-size: 17vw;
          padding-bottom: 0.2em;
        }
      }
    </style>
    <figure>
      <h1><em>${names.first}</em> ${names.last}</h1>
      <aside>${names.title}</aside>
    </figure>
  `,
});
