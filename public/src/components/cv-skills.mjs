/**
 * @file CV Contact link component.
 */
import { html } from '../../web_modules/hybrids.js';

/**
 * Format a given ISO8601 embedded date as a number of years.
 *
 * @param {string} fromDate
 *   The date to difference from.
 * @param {string} toDate
 *   The date to difference to.

 * @returns {string}
 *   Formatted date, in years with single decimal precision.
 */
function formatDateDiff(fromDate, toDate) {
  fromDate = new Date(fromDate);
  toDate = new Date(toDate);

  const timeDiff = Math.abs(toDate.getTime() - fromDate.getTime());
  return Math.round((timeDiff / (1000 * 3600 * 24 * 365)) * 10) / 10;
}

/* Show a tag item's specifics */
function showItem(selectId) {
  return (host, event) => {
    // Deselect if already selected.
    if (host.selectedItem == selectId) {
      host.selectedItem = '';
    } else {
      host.selectedItem = selectId;
    }

    // Don't do anything else.
    event.preventDefault();
  }
}

export default ({ valid: { date: validDate }, skills }) => ({
  selectedItem: '',

  render: ({ selectedItem }) => html`
    <style>
      a {
        text-decoration: none;
      }

      a:hover {
        font-weight: bolder;
      }

      a:link, a:visited {
        color: var(--highlight-low);
      }

      a:active {
        color: var(--active-color);
      }

      a.tag {
        display: inline-block;
        color: var(--text-inverse);
        background-color: var(--highlight-color);
        padding: 0.3em 0.6em;
        border-radius: 0.4em;
        margin: 0.2em;
        white-space: nowrap;
        font-size: 1.2em;
        font-family: 'Inter', sans-serif;
      }

      a.tag.selected {
        background-color: var(--active-color);
      }

      details {
        margin-top: 1em;
        font-size: 20px;
        color: var(--text-color);
        font-family: 'Inter', sans-serif;
      }

      details summary {
        font-weight: bold;
        cursor: pointer;
      }

      details ul {
        font-size: 0.9em;
      }

      @media only print {
        details {
          display: none;
        }

        a.tag {
          border: 1px solid var(--highlight-color);
          background: none;
          color: var(--text-color);
        }
      }
    </style>
    ${skills.map(({ id, title }) => html`
      <a
        href="#"
        class=${{tag: true, selected: id === selectedItem}}
        onclick=${showItem(id)}
      >
        ${title}
      </a>
    `)}
    ${skills.map(({ id, date, description, specifics }) => html`
      <details style=${id === selectedItem ? {} : { display: 'none' }}>
        <summary>${formatDateDiff(date.start, date.end || validDate)} years &ndash; ${description}</summary>
        <ul>${specifics.map((item) => html`<li>${item}</li>`)}</ul>
      </details>
    `)}
  `,
});
