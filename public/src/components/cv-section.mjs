/**
 * @file CV Section component.
 */
import { html } from '../../web_modules/hybrids.js';

export default {
  title: '',

  render: ({ title }) => html`
    <style>
      section { padding: 1em 2em; }
      h2 {
        border-bottom: 0.1em solid var(--text-color);
        color: var(--text-color);
        font-family: 'Inter', sans-serif;
        text-transform: uppercase;
        padding-left: 0.2em;
        font-weight: 300;
      }

      @media only print {
        section { padding: 1em; }
      }
    </style>
    <section>
      ${title && html`<h2>${title}</h2>`}
      <slot></slot>
    </section>
  `,
};
