/**
 * @file CV Work Experience link component.
 */
import { html } from '../../web_modules/hybrids.js';

/**
 * Format a given ISO8601 embedded date as a year range with count.
 *
 * @param {string} fromDate
 *   The date to difference from.
 * @param {string} toDate
 *   The date to difference to.
 *
 * @returns {string}
 *   Formatted date range, in years.
 */
function formatDateRange(fromDate, toDate, validDate) {
  var fromYear = fromDate.split('-')[0];
  var toYear;
  if (typeof toDate === 'undefined') {
    toDate = validDate;
    toYear = 'Present';
  } else {
    toYear = toDate.split('-')[0];
  }

  return fromYear + ' - ' + toYear;
}

export default ({ valid: { date: validDate }, experience }) => ({
  render: () => html`
    <style>
      a { text-decoration: none; }
      a:link, a:visited { color: var(--link-color); }
      a:active { color: var(--link-active); }

      article:first-of-type > header {
        margin-top: 0;
      }

      article {
        display: grid;
        padding-bottom: 1em;
        position: relative;
        font-family: var(--body-font);
        grid-template-columns: 130px 1fr;
        grid-template-areas: "date header" "....... description";
      }

      article::after {
        position: absolute;
        content: "";
        height: 100%;
        border-left: 1px solid var(--text-color);
        left: 129px;
      }

      article .date {
        grid-area: date;
        font-size: 0.8em;
        z-index: 1;
        text-align: right;
        margin-top: 6px;
        color: var(--text-color);
      }

      article .date::after {
        display: inline-block;
        content: "";
        width: 1em;
        height: 1em;
        border-radius: 0.6em;
        box-shadow:
          0px 0px 0px 0.25em var(--text-inverse),
          inset 0 0 0 1000px var(--text-color);
        margin-right: -0.45em;
      }

      article header {
        grid-area: header;
        margin-left: 1em;
        color: var(--text-color);
      }

      article header aside {
        margin-bottom: 1em;
        font-size: smaller;
        font-style: italic;
      }

      article .description {
        grid-area: description;
        margin-left: 1em;
        font-size: 0.8em;
        color: var(--text-color);
      }

      h4 {
        margin: 0;
        margin-bottom: 0.2em;
        font-size: 1.3em;
        border-bottom: 1px dashed var(--link-color);
      }

      @media only print {
        article {
          grid-template-columns: 110px 1fr;
        }

        article::after {
          left: 109px;
        }

        article header {
          display: flex;
        }

        article header aside {
          font-size: larger;
          margin-left: 2em;
          font-style: italic;
          text-align: right;
          flex: 1;
        }
      }
    </style>
    ${experience.map(({ business, employment }) => html`
      <article>
        <label class="date">
          ${formatDateRange(employment.date.start, employment.date.end, validDate)}
        </label>
        <header>
          <h4>
            <a href="https://${business.contact.website}">
              ${business.name}
            </a>
          </h4>
          <aside>${employment.title}</aside>
        </header>
        <div class="description">${employment.description}</div>
      </article>
    `)}
  `,
});
