/**
 * @file Index for all CV components
 */
import Section from './cv-section.mjs';
import Name from './cv-name.mjs';
import Contact from './cv-contact.mjs';
import Expertise from './cv-expertise.mjs';
import Skills from './cv-skills.mjs';
import Experience from './cv-experience.mjs';

export default (cvData) => ({
  'cv-section': Section,
  'cv-name': Name(cvData),
  'cv-contact': Contact(cvData),
  'cv-expertise': Expertise(cvData),
  'cv-skills': Skills(cvData),
  'cv-experience': Experience(cvData),
});

