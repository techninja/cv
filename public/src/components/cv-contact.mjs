/**
 * @file CV Contact link component.
 */
import { html } from '../../web_modules/hybrids.js';

export default ({ contact }) => ({
  render: () => html`
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <style>
      a {
        text-decoration: none;
        margin-bottom: 0.2em;
        display: block;
        font-size: 1.2em;
      }

      a:link, a:visited {
        color: var(--link-color);
      }

      .email {
        margin-bottom: 0.7em;
      }

      a i em {
        font-family: 'Comfortaa', sans-serif;
        font-style: normal;
      }

      @media only screen and (min-width: 801px) {
        /* Hide contact link icons on large screens */
        aside#contact a i::before { content: ""; }
      }

      /* print only */
      @media only print {
         aside#contact a i::before { content: ""; }
      }

      @media only screen and (max-width: 800px) {
        /* Contact Icons ONLY */
        aside#contact {
          display: grid;
          font-size: 3em;
          grid-template-columns: repeat(3, 33%);
          grid-gap: 10px;
          width: 90%;
          padding-top: 0.5em;
        }

        a, a.email {
          margin-bottom: 0;
        }

        aside#contact a i em {
          display: none;
        }
      }

      @media only screen and (max-width: 700px) {
        aside#contact {
          padding-top: 0;
          display: block;
          width: 100%;
          margin-bottom: 20px;
          font-size: 12vw;
        }

        aside#contact a { display: inline; }
      }
    </style>
    <aside id="contact">
      ${contact.phone && html`
        <a href="tel:${contact.phone.country}-${contact.phone.area}-${contact.phone.group}-${contact.phone.specific}">
          <i class="fa fa-phone-square"><em>${contact.phone.area}.${contact.phone.group}.${contact.phone.specific}</em></i>
        </a>
      `}
        <a class="email" href="mailto:${contact.email}">
          <i class="fa fa-envelope-square"><em>${contact.email}</em></i>
        </a>
      ${contact.social.twitter && html`
        <a href="https://twitter.com/${contact.social.twitter}">
          <i class="fa fa-twitter-square"><em>@${contact.social.twitter}</em></i>
        </a>
      `}
      ${contact.social.github && html`
        <a href="https://github.com/${contact.social.github}">
          <i class="fa fa-github-square"><em>github.com/${contact.social.github}</em></i>
        </a>
      `}
      ${contact.social.linkedin && html`
        <a href="https://linkedin.com/in/${contact.social.linkedin}">
          <i class="fa fa-linkedin-square"><em>linkedin.com/in/${contact.social.linkedin}</em></i>
        </a>
      `}
    </aside>
  `,
});
