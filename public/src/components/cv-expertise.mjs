/**
 * @file CV Expertise bar graph component.
 */
import { html } from '../../web_modules/hybrids.js';

export default ({ expertise }) => ({
  render: () => html`
    <style>
      .legend {
        border-top: 2px solid var(--highlight-color);
        font-family: sans-serif;
        width: 100%;
        display: grid;
        grid-template-columns: 1fr 20px 1fr;
      }

      .legend em {
        border: 0 solid var(--highlight-color);
        font-style: normal;
        color: var(--text-color);
        padding-top: 2px;
      }

      .legend em.start {
        border-left-width: 2px;
        padding-left: 0.3em;
      }

      .legend em.mid {
        border-left-width: 2px;
        padding-left: 0.3em;
      }

      .legend em.end {
        border-right-width: 2px;
        text-align: right;
        padding-right: 0.3em;
      }

      figure {
        margin: 0;
        font-size: 2em;
        font-family: 'Inter', sans-serif;
      }

      figure div {
        background-color: var(--highlight-color);
        height: 1em;
        margin-top: 10px;
      }

      figure div label {
        margin: 0;
        color: var(--text-inverse);
        font-size: 0.6em;
        padding-left: 0.2em;
        vertical-align: super;
        white-space: nowrap;
      }

      @media only print {
        figure div {
          background: none;
          border: 1px solid var(--highlight-color);
        }

        figure div label {
          color: var(--text-color);
        }
      }
    </style>
    <aside class="legend">
      <em class="start">0</em>
      <em class="mid">5</em>
      <em class="end">10</em>
    </aside>
    ${expertise.map(({ name, value }) => html`
      <figure>
        <div style="width: ${value}%">
          <label>
            ${name}
          </label>
        </div>
      </figure>
    `)}
  `,
});
