import { html } from './web_modules/hybrids.js';
export default {
  __preface:
    'This is the JSON data for my personal CV! View a formatted version @ cv.tn42.com',
  valid: {
    date: '2023-05-16',
  },
  names: {
    real: 'James Todd',
    first: 'James',
    last: 'Todd',
    nick: 'Tech Ninja',
    title: 'Senior Full Stack Developer',
    pronouns: 'he/him',
  },
  picture: 'ninjaeyes.png',
  contact: {
    phone: {
      country: 1,
      area: 530,
      group: 217,
      specific: 3842,
    },
    email: 'james@tn42.com',
    social: {
      twitter: 'techninja42',
      github: 'techninja',
      linkedin: 'techninja',
    },
  },
  expertise: [
    {
      name: 'JavaScript',
      value: 95,
    },
    {
      name: 'TypeScript',
      value: 88,
    },
    {
      name: 'CSS/SASS',
      value: 85,
    },
    {
      name: 'SQL',
      value: 75,
    },
    {
      name: 'HTML',
      value: 100,
    },
    {
      name: 'REST API',
      value: 90,
    },
    {
      name: 'AWS / Cloud',
      value: 70,
    },
  ],
  skills: [
    {
      title: 'Node.JS/Typescript',
      id: 'node',
      date: {
        start: '2011-11-12',
      },
      description:
        'Serverside TypeScript/JavaScript via Node.js 0.10-20.x CommonJS/ES6/ES 2020+',
      specifics: [
        html`Helped implement a GraphQL "backend for frontends" to supply 40+
        clientside applications with movie, show, and clip data for NBC on web,
        roku, ios, android and more for millions of customers worldwide.`,
        html`Created an HTTP protocol based
          <a href="https://github.com/techninja/cncserver"
            >ReSTful API server for CNC robotics</a
          >, powered by Node.js, with
          <a href="https://github.com/techninja/cncserver/blob/master/API.md"
            >fully documented API</a
          >
          that went to the
          <a
            href="http://www.nytimes.com/2013/04/24/science/sylvia-todd-science-star-tinkers-with-the-idea-of-growing-up.html?pagewanted=all"
            >White House on a Raspberry Pi</a
          >
          with
          <a
            href="http://sylviashow.com/blog/super-awesome-sylvia/2013/04/27/my-crazy-dc-adventure"
            >my son</a
          >
          in only 2 months.`,
        html`Melded that same
          <a href="https://github.com/techninja/cncserver"
            >CLI Node.js API server</a
          >
          and clientside app into a
          <a href="https://github.com/techninja/robopaint"
            >stand-alone desktop Electron application</a
          >
          with automated build system for Mac, Windows & Linux.`,
        html`Full Clientside managed translations, realtime swappable and AJAX
          ready, based on <a href="http://i18next.com/">i18Next</a> JSON base
          path string. To compliment, also created a node triggered python
          script for
          <a href="https://github.com/techninja/robopaint-translator"
            >automated creation of base translations files</a
          >
          from root english to other languages.`,
        html`Created a realtime streaming
          <a href="http://github.com/techninja/ninjanode">HTML5 game</a> for
          mobile and desktops utilizing Node.js and websockets.`,
      ],
    },
    {
      title: 'Clientside JavaScript',
      id: 'js',
      date: {
        start: '2009-09-15',
      },
      description:
        'Clientside JavaScript, with and without frameworks like React, jQuery, and others.',
      specifics: [
        html`Fluent in both classic and ES2020+ in browser modules and compiled
        bundled apps for cross platform and legacy compatiblity.`,
        html`Implementation, creation, and use of dozens of companion plugins
        and direct plugin customization for applications.`,
        html`Full ReSTful API based applications using custom written API
        handlers, properly utilizing headers for both methods and secure OAuth
        signed requests.`,
      ],
    },
    {
      title: 'React.js',
      id: 'react',
      date: {
        start: '2016-09-15',
      },
      description: 'React.js UI component framework',
      specifics: [
        html`Created React component driven web-app frontend on a team for the
          <a
            href="https://www.fourkitchens.com/blog/development/building-new-react-frontend-pri-org/"
            >PRI.org homepage relaunch</a
          >
          with accompanying serverside proxy render pipelines.`,
        html`Lead a team in building out complete front end for a complex
        microservices stack serving FHIR medical resources and data management
        UI React component library served via Gatsby.`,
      ],
    },
    {
      title: 'Microservices',
      id: 'services',
      date: {
        start: '2016-01-03',
      },
      description:
        'Architecture and optimization & infrastructure as code for microservices via AWS/Azure',
      specifics: [
        html`Built a MongoDB change stream extender via AWS stack utilizing SNS,
        SQS, DynamoDB, Lambda, API Gateway, Cognito and more in Terraform. Then
        transitioned to TypeScript & CDK, improving every element of deploys and
        developer experience.`,
        html`Built my own bundler to give Node 14.x in Lambda full support for
        ES6+ modules natively without compilation step (and prevents disabling
        of the console code editor).`,
        html`Built dozens of Lambda based API Gateway Restful handlers,
        utilizing Cognito user pools and groups for authentication and DynamoDB
        as data storage.`,
        html`Commitment to horizontal scaling throughput and latency management
        through reporting tooling, automated testing, and latest standards for
        low latency/memory usage.`,
      ],
    },
    {
      title: 'Embedded Devices',
      id: 'arduino',
      date: {
        start: '2009-01-15',
      },
      description: 'Embedded Linux/Arduino development',
      specifics: [
        html`Device development for basic IOT objects via
          <a href="https://www.particle.io/">Particle</a>`,
        html`Co-wrote and developed all hardware projects for Arduino in
          <a href="http://superawesomebook.com"
            >"Sylvia's Super-Awesome Project Book"</a
          >`,
        html`Development and experimentation for embedded prototypes utilizing
        Raspberry Pi 1, 2 and 3 incorporating physical device interation and
        touchscreen`,
        html`<a href="http://www.moddable.com/moddable-zero.php"
            >Moddable Zero XS JavaScript</a
          >
          dev for low power/cost ESP8266 wireless/restfull applications`,
      ],
    },
    {
      title: 'MySQL/MariaDB',
      id: 'sql',
      date: {
        start: '2006-06-20',
      },
      description:
        'MySQL/MariaDB 5.x, Redis, and other general database and data storage architechture planning and query management/optimization',
      specifics: [
        html`Administration of schemas, tables, cache and general query
        debugging.`,
        html`SH/BASH shell scripting for DB backup, export and import.`,
      ],
    },
    {
      title: 'Linux',
      id: 'linux',
      date: {
        start: '2008-02-22',
      },
      description: 'CentOS / Ubuntu / *nix',
      specifics: [
        html`Shell scripting for backups, general purpose automation and
        management.`,
        html`Open Stack management with Apache projects, SSH, Jenkins CI,
        Varnish tuning, and lots more.`,
      ],
    },
  ],
  experience: [
    {
      business: {
        name: 'Ripple Science',
        contact: {
          website: 'ripplescience.com',
        },
      },
      employment: {
        title: 'Senior Software Developer',
        date: {
          start: '2021-11-01',
          end: '2023-04-27',
        },
        description: html`
          Lead a small team in supporting existing software stack for 25+
          enterprise clients utilizing TypeScript/JavaScript for React.js,
          MongoDB, and Meteor on Kuburnetes, developing new features and tests,
          while building a new modular microframework stack through AWS and CDK
          to manage transition data through queues and integration into FHIR
          standard medical information resources.
        `,
      },
    },
    {
      business: {
        name: 'Express-Scripts',
        contact: {
          website: 'express-scripts.com',
          phone: '800-367-2884 (company 36032)',
        },
      },
      employment: {
        title: 'Full Stack Engineer',
        date: {
          start: '2020-11-01',
          end: '2021-11-01',
        },
        description: html`
          Developing with Node.js, JavaScript/Typescript & PHP/Drupal for a
          suite of high traffic sites, developing new features, automating build
          tasks, developer tooling, functional/regresssion testing, CI builds
          via Github Actions, and unifying local development for a team of over
          10.
          <ul>
            <li>
              <a href="https://evernorth.com">Evernorth (Cigna's B2B Brand)</a>
            </li>
            <li><a href="https://mymatrixx.com">myMatrixx</a></li>
            <li><a href="https://express-scripts.com">Express-Scripts</a></li>
          </ul>
        `,
      },
    },
    {
      business: {
        name: 'Four Kitchens',
        contact: {
          website: 'fourkitchens.com',
          phone: '512.454.6659',
        },
      },
      employment: {
        title: 'Full Stack Developer',
        date: {
          start: '2015-11-05',
          end: '2020-11-05',
        },
        description: html`
          Developing with React/Node.js, JavaScript, PHP/Drupal for multiple
          large projects as both engineer and dev team lead, developing new
          features, JavaScript interaction/applications, code review and
          architecture, theming for mobile and responsive first, CI build
          scripts, automating deploys/tests, and lots more:
          <ul>
            <li><a href="https://nbc.com">NBC GraphQL content backend</a></li>
            <li><a href="https://mdedge.com">MD Edge</a></li>
            <li>
              <a href="https://www.globalacademycme.com/"
                >Global Academy for Medical Education</a
              >
            </li>
            <li><a href="https://www.comsoc.org/">IEEE ComSoc</a></li>
            <li><a href="https://www.ithaca.edu/">Ithaca College</a></li>
            <li>
              <a href="https://www.pri.org/">Public Radio International</a>
            </li>
            <li><a href="http://editvr.io">EditVR</a></li>
          </ul>
        `,
      },
    },
    {
      business: {
        name: 'Storebound',
        contact: {
          website: 'storebound.com',
          phone: '646.459.4300',
        },
      },
      employment: {
        title: 'Lead Developer',
        date: {
          start: '2015-08-01',
          end: '2017-08-01',
        },
        description: html`Working as lead architect, project manager & developer
          for a
          <a href="https://github.com/PancakeBot/PancakePainter"
            >cross platform desktop application</a
          >
          written in JavaScript through Electron to allow users to design
          graphics for their Pancake drawing robot. Open source from the start,
          all development and discussion in the open.`,
      },
    },
  ],
};
